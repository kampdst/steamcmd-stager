SteamCMD Stager
===============

_Work in Progress_!

This project aims to use [steamcmd](https://developer.valvesoftware.com/wiki/SteamCMD) to detect when an app (eg. dedicated game server) has been released, and if needed, pull the latest files down. 


Usage
-----

``` bash
# build docker image
docker build -t steamcmd-stager:dev .

# run stager script
# change env:app_id to the steam app id needed
# change env:app_path_id to a colon delimited string to access a unique id in app_info (such as release timestamp)
# create volume:/home/steam/app to persist app data
docker run -it --rm \
	-e app_id=343050 \
	-e app_path_id="depots:branches:public:timeupdated"
	-v app-data:/home/steam/app
	steamcmd-stager:dev
```


Development
-----------

To test script changes quickly, mount the script directly:

``` bash
docker run -it --rm \
	-e app_id=343050 \
	-e app_path_id="depots:branches:public:timeupdated"
	-v app-data:/home/steam/app
	-v $PWD/src/updater.py:/home/steam/updater.py
	steamcmd-stager:dev
```


Licenses
--------

See [LICENSE](./LICENSE) for details on this project's licensing.

Code snippets and files from [steamcmd/api](https://github.com/steamcmd/api) are used under MIT license.
Code snippets from [marceldev89/a3update.py](https://gist.github.com/marceldev89/12da69b95d010c8a810fd384cca8d02a) are used under MIT liecense.