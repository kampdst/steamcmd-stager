FROM ubuntu:20.04

ARG PUID=1000

# ENV for steam app id
ENV app_id=

# ENV for unique id path
ENV app_path_id=

RUN apt update \
	&& apt upgrade -y \
	&& apt install -y wget python3 lib32stdc++6 lib32gcc1 \
	# && apt purge -y wget \
	&& apt clean autoclean \
	&& apt clean \
	&& apt autoremove -y \
	&& rm -rf /var/lib/apt/lists/*


RUN useradd -u $PUID -m steam
RUN mkdir -p /home/steam/steamcmd \
	&& mkdir -p /home/steam/app
RUN wget -qO- https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz | tar xvz -C /home/steam/steamcmd

# update steamcmd
COPY ./src /home/steam
RUN /home/steam/steamcmd/steamcmd.sh +login anonymous +quit \
	&& chmod +x /home/steam/updater.py \
	&& chown steam:steam -R /home/steam/

USER steam
WORKDIR /home/steam

CMD ["python3", "/home/steam/updater.py"]
