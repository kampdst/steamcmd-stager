#!/usr/bin/python3

import os
import subprocess
import vdf


# CONFIGURATIONS
# ##############
STEAM_CMD = "/home/steam/steamcmd/steamcmd.sh"
APP_DIR = "/home/steam/app"
APP_ID = os.getenv('app_id')
APP_PATH_ID = os.getenv('app_path_id')


def call_steamcmd(params):
	output = subprocess.check_output("{} {}".format(STEAM_CMD, params), shell=True)
	return output.decode("UTF-8")

def check_staged_data(path, release):
	file = path + "/latest.txt"

	try:
		with open(file, 'r') as f:
			data = f.readline()
	except FileNotFoundError:
		return False

	if data.strip() == release:
		return True
	return False


def clean_app_info_print(output, app_id):
	# from steamcmd/api/src/run.py
	output = output[output.find('"' + app_id + '"') :]
	output = "}".join(output.split("}")[:-1])
	output += "}"
	return output


def release_in_play(path, release):
	file = path + "/live.txt"

	try:
		with open(file, 'r') as f:
			data = f.readline()
	except FileNotFoundError:
		return False

	if data.strip() == release:
		return True
	return False


def unique_id_from_path(path, data):
	elements = path.split(':')

	for e in elements:
		data = data[e]

	return(data)


def update_app_info(app_id):
	params = " +login anonymous"
	params += " +app_info_print {}".format(app_id)
	params += " +quit"

	output = clean_app_info_print(call_steamcmd(params), app_id)
	return output


def update_server(app_id, location):
	params = " +force_install_dir {}".format(location)
	params += " +login anonymous"
	params += " +app_update {} validate".format(app_id)
	params += " +quit"

	output = call_steamcmd(params)

	if "Success!" in output:
		return True
	else:
		return False

if __name__ == "__main__":
	print("Staging app ({}) with SteamCMD".format(APP_ID))
	print("Access app info for ({})".format(APP_ID))
	output = update_app_info(APP_ID)
	data = vdf.read(output)[APP_ID]
	code = unique_id_from_path(APP_PATH_ID, data)
	newdir = APP_DIR + '/' + code
	
	if check_staged_data(APP_DIR, code):
		print("Skipping as release ({}) stored in ({}) has already been verified.".format(code, newdir))
		exit()

	try:
		os.makedirs(newdir)
	except FileExistsError:
		if not release_in_play(APP_DIR, code):
			print("Verifing release ({}) stored in ({})".format(code, newdir))
		else:
			print("Skipping verifing of release ({}) stored in ({}) because this release is currently in use.".format(code, newdir))
			exit()
	else:
		print("Downloading release ({}) to ({})".format(code, newdir))
	
	if not update_server(APP_ID, newdir):
		print("Error!")
	else:
		file = APP_DIR + "/latest.txt"

		with open(file, 'w') as f:
			data = f.write(code)
