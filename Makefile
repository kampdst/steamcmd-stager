REGISTRY = kampdst
PROJECT = steamcmd-stager
VCS_HASH := $(shell git rev-parse --short HEAD)
.PHONY: build build-nocache release

build:
	docker build -t $(REGISTRY)/$(PROJECT):$(VCS_HASH) --rm .

build-nocache:
	docker build -t $(REGISTRY)/$(PROJECT):$(VCS_HASH) --no-cache --rm .

release: build
# 	how to check if the release is ready?
	docker push $(REGISTRY)/$(PROJECT):$(VCS_HASH)
